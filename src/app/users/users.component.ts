import {Component, OnInit} from '@angular/core';
import {collectExternalReferences} from "@angular/compiler";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  user = {first: '', last: ''};
  delete = 0;
  c = 0;
  index;
  edit = 0;
  user2 = {first: '', last: ''};
  no_display_add_group = 0;
  // use_for_cancel = {first: '', last: ''};
  copy_of_users = [];
  oneClick = 0;
  users = [
    {first: 'majid', last: 'nayyeri'},
    {first: 'mohsen', last: 'ashoori'}
  ];

  constructor() {
  }


  onAddUserButtonClick() {
    if (this.user.first != '' && this.user.last != '') {
      this.users.push(this.user);
      this.user = {first: '', last: ''};
      this.c = 0;
    }
  }



  onRowClick(user) {
    this.user2 = user;
    this.edit = 1;
    if (this.oneClick == 0) {
      this.copy_of_users = JSON.parse(JSON.stringify(this.users));
      this.oneClick = 1;
    } else{
      if((JSON.stringify(this.copy_of_users))!=(JSON.stringify(this.users))){
        for(var i = 0; i < this.copy_of_users.length; i++){
          if((JSON.stringify(this.copy_of_users[i]))==(JSON.stringify(user))){
            this.users=this.copy_of_users;
            this.oneClick=0;
          }
        }
      }
    }

    this.no_display_add_group = 1;
  }

  onEditBtnClick() {
    this.oneClick = 0;
    this.c = 0;
    this.no_display_add_group = 0;
    this.edit = 0;
    this.user2 = {first: '', last: ''};
  }

  onDeleteBtnClick() {
    this.oneClick = 0;
    this.c = 0;
    this.no_display_add_group = 0;
    for (var i = 0; i < this.users.length; i++) {
      if (this.users[i] == this.user2) {
        this.users.splice(i, 1);
        this.edit = 0;
      }
    }
  }

  onAddButtnClick() {
    this.c = 1;
  }

  onClickButtnClose() {
    this.user = {first: '', last: ''};
    this.c = 0;
  }

  onCancelBtnClick() {
    this.oneClick = 0;
    this.users = this.copy_of_users;
    this.copy_of_users = [];
    this.c = 0;
    this.no_display_add_group = 0;
    this.edit = 0;
    this.user = {first: '', last: ''};
    this.user2 = {first: '', last: ''};
  }

  ngOnInit() {
  }

  onSave() {
    this.c = 0;
    this.edit = 0;
  }

}
